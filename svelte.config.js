import preprocess from 'svelte-preprocess';
import atImport from 'postcss-import';
import autoprefixer from 'autoprefixer';

export default {
  preprocess: [
    preprocess({
      scss: {
        // We can use a path relative to the root because
        // svelte-preprocess automatically adds it to `includePaths`
        // if none is defined.
        prependData: `@import 'lib/styles/variables.scss';`,
      },
      postcss: {
        plugins: [autoprefixer(), atImport()],
      },
    }),
  ],
};
