# Speech

## Overview

TODO


## Usage

### Add a TrainerButton that trigger a fullscreen

```svelte
<script>
  import { fullscreen } from 'inca-utils/api';
  import { faExpand } from '@fortawesome/free-solid-svg-icons';
</script>


<TrainerButton on:click={fullscreen} label="Fullscreen">
  <Fa icon={faExpand} />
</TrainerButton>
```