# Action Button

## Overview

Action buttons are designed to allow the non-human subject to interact with the application.

### Use Cases

- An exit button used by the subject when it want to stop playing.
- A ready button to start the next level of a game.

## Usage

```svelte
<script lang="ts">
  import { ActionButton } from 'inca-utils';
</script>

<!-- Ready button -->
<ActionButton
  on:click={ready}
  mode="ready"
/>

<!-- Exit button -->
<ActionButton
  on:click={exit}
  mode="exit"
/>

<!-- Styling -->
<ActionButton
  mode="ready"
  --width="32px"
  --background-color="green"
  --color="black"
/>
```

## Props

| Prop name  | Type       | Default  | Description  |
| ---------- | ---------- | -------- | --------------- |
| `on:click`   | `function` | `undefined` | Trigger a click event. |
| `mode` | `ready \| exit` | required | The button type. |

## Styling

You can customize the component using these props:
* `--width` Set the width
* `--color` Set the icon color
* `--background-color` Set the background color

## Source

[View source code on GitLab](https://gitlab.com/shockerqt/inca-utils/-/blob/main/lib/components/ActionButton.svelte)
